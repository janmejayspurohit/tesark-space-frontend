import React, { useEffect, useState } from "react";
import Button from "./components/css/Button.js";
import { connectWallet, getCurrentWalletConnected, getNfts, mintNFT } from "./utils/interact.js";

const Minter = () => {
  const [walletAddress, setWallet] = useState("");
  const [status, setStatus] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [url, setURL] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isGetNftLoading, setIsGetNftLoading] = useState(false);
  const [nftData, setNFTData] = useState({});

  useEffect(() => {
    const fn = async () => {
      const { address, status } = await getCurrentWalletConnected();
      setWallet(address);
      setStatus(status);
      addWalletListener();
    };
    fn();
  }, []);

  function addWalletListener() {
    if (window.ethereum) {
      window.ethereum.on("accountsChanged", accounts => {
        if (accounts.length > 0) {
          setWallet(accounts[0]);
          setStatus("👆🏽 Write a message in the text-field above.");
        } else {
          setWallet("");
          setStatus("🦊 Connect to Metamask using the top right button.");
        }
      });
    } else {
      setStatus(
        <p>
          <a target="_blank" rel="noopener noreferrer" href={`https://metamask.io/download.html`}>
            You must install Metamask
          </a>
        </p>
      );
    }
  }
  const connectWalletPressed = async () => {
    const walletResponse = await connectWallet();
    setStatus(walletResponse.status);
    setWallet(walletResponse.address);
  };

  const onMintPressed = async () => {
    setIsLoading(true);
    const { status } = await mintNFT(url, name, description);
    setStatus(status);
    setIsLoading(false);
  };

  const getNftsPressed = async () => {
    getNfts({ setNFTData, setIsGetNftLoading });
  };

  const LabelledData = ({ label, value }) => (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        width: "800px",
        height: "20px",
        justifyContent: "space-between",
      }}
    >
      <h4 style={{ color: "#595959" }}>{label}:</h4>
      <h4 style={{ color: "#595959" }}>{value}</h4>
    </div>
  );

  return (
    <div style={{ textAlign: "-webkit-center" }}>
      <div style={{ textAlign: "center", width: "800px" }}>
        <Button id="walletButton" onClick={connectWalletPressed}>
          {walletAddress.length > 0 ? "Connected to: " + walletAddress : <span>Connect Wallet</span>}
        </Button>
        <br />
        <h1 id="title">{`🧙‍♂️ NFT Minter`}</h1>
        <form
          style={{
            backgroundColor: "#337ab7",
            padding: "10px",
            borderRadius: "5px",
          }}
        >
          <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between", margin: "10px" }}>
            <label
              style={{
                fontSize: "1.2rem",
                fontWeight: "bold",
                color: "white",
              }}
            >
              {"Link to asset: "}
            </label>
            <input
              value={url}
              style={{ width: "550px", height: "30px", borderRadius: "5px", borderColor: "#f2f2f2" }}
              onChange={event => setURL(event.target.value)}
              placeholder="e.g. https://gateway.pinata.cloud/ipfs/<hash>"
              required
            />
          </div>
          <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between", margin: "10px" }}>
            <label
              style={{
                fontSize: "1.2rem",
                fontWeight: "bold",
                color: "white",
              }}
            >
              {"Name: "}
            </label>
            <input
              value={name}
              style={{ width: "550px", height: "30px", borderRadius: "5px", borderColor: "#f2f2f2" }}
              onChange={e => setName(e.target.value)}
              placeholder="Name of the NFT"
              required
            />
          </div>
          <div style={{ display: "flex", alignItems: "center", justifyContent: "space-between", margin: "10px" }}>
            <label
              style={{
                fontSize: "1.2rem",
                fontWeight: "bold",
                color: "white",
              }}
            >
              {"Description: "}
            </label>
            <input
              value={description}
              style={{ width: "550px", height: "30px", borderRadius: "5px", borderColor: "#f2f2f2" }}
              onChange={e => setDescription(e.target.value)}
              placeholder="Description of the NFT"
              required
            />
          </div>
        </form>

        <Button id="mintButton" disabled={!url || isLoading} onClick={onMintPressed}>
          {!url ? "Please uplaod a file!" : isLoading ? "Minting..." : "Mint NFT"}
        </Button>

        <Button id="mintButton" onClick={getNftsPressed} disabled={isGetNftLoading}>
          {isGetNftLoading ? "Fetching..." : "Get NFT"}
        </Button>

        <p id="status">{status}</p>
      </div>
      <hr />
      {nftData.blockHash && (
        <div
          style={{
            width: "90%",
            height: "250px",
            overflow: "scroll",
            backgroundColor: "#F2F2F2",
            padding: "10px",
            borderRadius: "5px",
          }}
        >
          <h2>This Wallet Totally Owns: {nftData.totalCount} NFTs</h2>
          <LabelledData label={"Block Hash"} value={nftData.blockHash} />
          {nftData.ownedNfts.map((nft, i) => (
            <div style={{ backgroundColor: "#B7ADCF", borderRadius: "5px", margin: "10px", padding: "10px" }}>
              <LabelledData id={i} label={"Description"} value={nft.description} />
              <a href={nft.metadata.image} target="_blank" rel="noopener noreferrer">
                <img style={{ width: "60px", height: "60px" }} src={nft.metadata.image} alt="No NFT Preview" />
              </a>
              <LabelledData id={i} label={"Token ID"} value={nft.id.tokenId} />
              <LabelledData id={i} label={"Token Type"} value={nft.id.tokenMetadata.tokenType} />
              <LabelledData id={i} label={"Contract Address"} value={nft.contract.address} />
            </div>
          ))}
        </div>
      )}
      {console.log("🚀 -> file: Minter.js -> line 499 -> nftData", nftData)}{" "}
    </div>
  );
};

export default Minter;
