import React, { useState } from "react";
import Initial from "./components/blocks/initial";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Room1 from "./components/blocks/room1";
import Room2 from "./components/blocks/room2";
// import Index from "./components/blocks/room1";
import ConnectWallet from "./components/metamask/ConnectWallet";
import Minter from "./Minter";

function App() {
  const [defaultWalletAccount, setDefaultWalletAccount] = useState(null);

  const RenderWallet = () => (
    <ConnectWallet defaultWalletAccount={defaultWalletAccount} setDefaultWalletAccount={setDefaultWalletAccount} />
  );

  const Home = () =>
    defaultWalletAccount ? <Initial defaultWalletAccount={defaultWalletAccount} /> : <RenderWallet />;

  const WalletWrapper = ({ children }) => (
    <>
      {defaultWalletAccount ? (
        <div style={{ backgroundColor: "green", display: "flex", justifyContent: "center" }}>
          <h3>{`Wallet with ID: ${defaultWalletAccount} is Connected!`}</h3>
        </div>
      ) : (
        <div style={{ backgroundColor: "red", display: "flex", justifyContent: "center" }}>
          <h3>{`Wallet not connected, please reload and connect!`}</h3>
        </div>
      )}
      {children}
    </>
  );

  return (
    <Router>
      <Routes>
        <Route
          exact
          path="/"
          element={
            <WalletWrapper>
              <Home />
            </WalletWrapper>
          }
        />
        <Route exact path="/room1" element={<Room1 />} />
        <Route exact path="/room2" element={<Room2 />} />
        <Route exact path="/connect" element={<RenderWallet />} />
        <Route exact path="/nft" element={<Minter />} />
      </Routes>
    </Router>
  );
}

export default App;
