import React, { useState } from "react";
import { ethers } from "ethers";

const ConnectWallet = props => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [userBalance, setUserBalance] = useState(null);
  const [connButtonText, setConnButtonText] = useState("Connect Wallet");

  const { defaultWalletAccount, setDefaultWalletAccount } = props;

  const connectWalletHandler = () => {
    if (!(window.ethereum && window.ethereum.isMetaMask))
      return setErrorMessage("Please install MetaMask browser extension to interact");
    window.ethereum
      .request({ method: "eth_requestAccounts" })
      .then(result => {
        accountChangedHandler(result[0]);
        setConnButtonText("Wallet Connected");
        getAccountBalance(result[0]);
        setErrorMessage(null);
      })
      .catch(error => {
        setErrorMessage(error.message);
      });
  };

  const accountChangedHandler = newAccount => {
    setDefaultWalletAccount(newAccount);
    getAccountBalance(newAccount.toString());
  };

  const getAccountBalance = account => {
    window.ethereum
      .request({ method: "eth_getBalance", params: [account, "latest"] })
      .then(balance => {
        setUserBalance(ethers.utils.formatEther(balance));
      })
      .catch(error => {
        setErrorMessage(error.message);
      });
  };

  const chainChangedHandler = () => window.location.reload();

  if (window.ethereum) {
    window.ethereum.on("accountsChanged", accountChangedHandler);
    window.ethereum.on("chainChanged", chainChangedHandler);
  }

  return (
    <div style={{ textAlign: "center" }}>
      <h2> {"Connect to MetaMask"} </h2>
      <button
        style={{
          color: "white",
          backgroundColor: "#337ab7",
          borderColor: "#337ab7",
          borderRadius: "4px",
          padding: "8px",
          fontWeight: "bold",
        }}
        onClick={connectWalletHandler}
      >
        {connButtonText}
      </button>
      {defaultWalletAccount && (
        <>
          <div className="accountDisplay">
            <h3>Address: {defaultWalletAccount}</h3>
          </div>
          <div className="balanceDisplay">{userBalance ? <h3>Balance: {userBalance}ETH</h3> : <p>Loading...</p>}</div>
        </>
      )}
      {errorMessage && <h3 style={{ color: "red" }}>{errorMessage}!</h3>}
    </div>
  );
};

export default ConnectWallet;
