import React from "react";
import { usePlane } from "use-cannon";
import { TextureLoader, RepeatWrapping, NearestFilter, LinearMipMapLinearFilter } from "three";
import { useLoader } from "@react-three/fiber";
import grass from "../assets/images/beach_soil.jpg";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { draco } from "drei";
import building from "../assets/building.glb";
import logo from "../assets/TESARK_Logo.glb";
import door from "../assets/door1.glb";

export const Ground = props => {
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }));
  const texture = new TextureLoader().load(grass);

  texture.magFilter = NearestFilter;
  texture.minFilter = LinearMipMapLinearFilter;
  texture.wrapS = RepeatWrapping;
  texture.wrapT = RepeatWrapping;
  texture.repeat.set(100, 100);

  const buildingGltf = useLoader(GLTFLoader, building, draco());
  const logoGltf = useLoader(GLTFLoader, logo, draco());
  const doorGltf = useLoader(GLTFLoader, door, draco());

  const buildingPrimitiveProps = {
    object: buildingGltf.scene,
    position: [0, 0, 0],
  };

  const logoPrimitiveProps = {
    object: logoGltf.scene,
    position: [-6, 2, 0],
    rotation: [0, Math.PI, Math.PI / 2],
    scale: [0.1, 0.5, 1],
  };

  const doorPrimitiveProps = {
    object: doorGltf.scene,
    position: [-1.88, 0.71, -5.9],
    scale: [7.28, 1.21, -1.4],
  };

  return (
    <>
      <mesh ref={ref} receiveShadow>
        <planeBufferGeometry attach="geometry" args={[100, 100]} />
        <meshStandardMaterial map={texture} attach="material" />
      </mesh>
      <primitive {...buildingPrimitiveProps} />
      <primitive {...logoPrimitiveProps} />
      <primitive {...doorPrimitiveProps} />
    </>
  );
};
