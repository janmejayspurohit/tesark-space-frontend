import React from "react";
import { Canvas } from "react-three-fiber";
import { Physics } from "use-cannon";
import { Room } from "./room1";
import { Player } from "./Player";

function Room1() {
  return (
    <Canvas shadowMap sRGB>
      <pointLight castShadow intensity={0.7} position={[100, 100, 100]} />
      <Physics gravity={[0, -200, 0]}>
        <Room position={[0, 0.2, 0]} />
        <Player position={[0, 0, 0]} />
      </Physics>
    </Canvas>
  );
}

export default Room1;
