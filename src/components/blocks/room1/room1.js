import React from "react";
import { usePlane } from "use-cannon";
import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { draco } from "drei";
import building from "../../../assets/hall_empty.glb";
import door from "../../../assets/door1.glb";
import Table from "../../../assets/Folding_Table.gltf";
import Table1 from "../../../assets/Folding_Table_1.gltf";
import Table2 from "../../../assets/Folding_Table_2.gltf";
import Table3 from "../../../assets/Folding_Table_3.gltf";
import BookTable from "../../../assets/book_shalf.glb";
import Chair from "../../../assets/Chair.glb";
import Chair1 from "../../../assets/Chair1.glb";
import Chair2 from "../../../assets/Chair2.glb";
import Chair3 from "../../../assets/Chair3.glb";
import Chair4 from "../../../assets/Chair4.glb";
import Chair5 from "../../../assets/Chair5.glb";
import Chair6 from "../../../assets/Chair6.glb";
import Laptop from "../../../assets/Laptop.glb";
import Laptop1 from "../../../assets/Laptop1.glb";
import Laptop2 from "../../../assets/Laptop2.glb";
import Laptop3 from "../../../assets/Laptop3.glb";
import Laptop4 from "../../../assets/Laptop4.glb";
import Laptop5 from "../../../assets/Laptop5.glb";
import Laptop6 from "../../../assets/Laptop7.glb";
import Sofa from "../../../assets/sofa.glb";
import Clock from "../../../assets/clock.glb";
import TableFan from "../../../assets/table_fan.glb";

export const Room = props => {
  // eslint-disable-next-line no-unused-vars
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }));
  const buildingGltf = useLoader(GLTFLoader, building, draco());
  const doorGltf = useLoader(GLTFLoader, door, draco());
  // const gui = new dat.GUI();
  const table1Gltf = useLoader(GLTFLoader, Table, draco());
  const table2Gltf = useLoader(GLTFLoader, Table1, draco());
  const table3Gltf = useLoader(GLTFLoader, Table2, draco());
  const table4Gltf = useLoader(GLTFLoader, Table3, draco());

  const bookTable = useLoader(GLTFLoader, BookTable, draco());
  const chairGlb = useLoader(GLTFLoader, Chair, draco());

  const chairGlb1 = useLoader(GLTFLoader, Chair1, draco());
  const chairGlb2 = useLoader(GLTFLoader, Chair2, draco());

  const chairGlb3 = useLoader(GLTFLoader, Chair3, draco());
  const chairGlb4 = useLoader(GLTFLoader, Chair4, draco());
  const chairGlb5 = useLoader(GLTFLoader, Chair5, draco());
  const chairGlb6 = useLoader(GLTFLoader, Chair6, draco());

  const laptop = useLoader(GLTFLoader, Laptop, draco());
  const laptop1 = useLoader(GLTFLoader, Laptop1, draco());
  const laptop2 = useLoader(GLTFLoader, Laptop2, draco());
  const laptop3 = useLoader(GLTFLoader, Laptop3, draco());
  const laptop4 = useLoader(GLTFLoader, Laptop4, draco());
  const laptop5 = useLoader(GLTFLoader, Laptop5, draco());
  const laptop6 = useLoader(GLTFLoader, Laptop6, draco());
  const sofa = useLoader(GLTFLoader, Sofa, draco());
  // const books = useLoader(GLTFLoader, Books, draco());
  const clock = useLoader(GLTFLoader, Clock, draco());
  const tableFan = useLoader(GLTFLoader, TableFan, draco());

  const buildingPrimitiveProps = {
    object: buildingGltf.scene,
    position: [0, 0, 0],
  };

  const doorPrimitiveProps = {
    object: doorGltf.scene,
    position: [0.03, -0.02, 6.7],
    rotation: [0, 1.6, 0],
  };
  const bookTablePrimitiveProps = {
    object: bookTable.scene,
    rotation: [-0.09, -1.49, -6.3],
    scale: [1, 1, 1],
    position: [2.64, 0.07, 6.34],
  };
  const table1PrimitiveProps = {
    object: table1Gltf.scene,
    position: [-1.43, 0.11, -6.5],
    scale: [2.51, 1, 1],
  };
  const table2PrimitiveProps = {
    object: table2Gltf.scene,
    position: [5, 0.11, -6.5],
    scale: [2.51, 1, 1],
  };
  const table3PrimitiveProps = {
    object: table3Gltf.scene,
    position: [-4.52, 0.11, -0.89],
    scale: [2.12, 1, 1],
    rotation: [0, 1.6, 0],
  };
  const table4PrimitiveProps = {
    object: table4Gltf.scene,
    position: [-4.52, 0.11, 2.88],
    scale: [2.12, 1, 1],
    rotation: [0, 1.6, 0],
  };
  const chairPrimitiveProps = {
    object: chairGlb.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-3.42, -0.8, -1.72],
    rotation: [0, -3.27, 0],
  };
  const chair1PrimitiveProps = {
    object: chairGlb1.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-3.42, -0.8, -0.26],
    rotation: [0, -3.27, 0],
  };

  const chair2PrimitiveProps = {
    object: chairGlb2.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-3.42, -0.8, 2.25],
    rotation: [0, -3.27, 0],
  };
  const chair3PrimitiveProps = {
    object: chairGlb3.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-3.42, -0.8, 3.65],
    rotation: [0, -3.27, 0],
  };

  const chair4PrimitiveProps = {
    object: chairGlb4.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-0.48, -0.78, -5.2],
    rotation: [0, 1.56, 0],
  };

  const chair5PrimitiveProps = {
    object: chairGlb5.scene,
    scale: [0.45, 0.37, -0.26],
    position: [-2.43, -0.78, -5.2],
    rotation: [0, 1.56, 0],
  };

  const chair6PrimitiveProps = {
    object: chairGlb6.scene,
    scale: [0.45, 0.37, -0.26],
    position: [5.17, -0.78, -5.2],
    rotation: [0, 1.56, 0],
  };

  const laptopPrimitiveProps = {
    object: laptop.scene,
    scale: [1, 1.3, 1.42],
    position: [-0.24, -0.15, -5.85],
    // rotation: [0, 1.56, 0]
  };

  const laptop1PrimitiveProps = {
    object: laptop1.scene,
    scale: [1, 1.3, 1.42],
    position: [-2.43, -0.15, -5.85],
    // rotation: [0, 1.56, 0]
  };

  const laptop2PrimitiveProps = {
    object: laptop2.scene,
    scale: [1, 1.3, 1.42],
    position: [5.37, -0.15, -5.85],
    // rotation: [0, 1.56, 0]
  };

  const laptop3PrimitiveProps = {
    object: laptop3.scene,
    scale: [1, 1.3, 1.42],
    position: [-3.99, -0.15, -0.47],
    rotation: [0, 1.56, 0],
  };

  const laptop4PrimitiveProps = {
    object: laptop4.scene,
    scale: [1, 1.3, 1.42],
    position: [-3.99, -0.15, -2.04],
    rotation: [0, 1.56, 0],
  };

  const laptop5PrimitiveProps = {
    object: laptop5.scene,
    scale: [1, 1.3, 1.42],
    position: [-3.99, -0.15, 1.73],
    rotation: [0, 1.56, 0],
  };

  const laptop6PrimitiveProps = {
    object: laptop6.scene,
    scale: [1, 1, 1],
    position: [-4.75, 1.11, 3.31],
    rotation: [6.28, 1.5, 0],
  };

  const sofaPrimitiveProps = {
    object: sofa.scene,
    rotation: [-0.09, 3, 0],
    position: [-2.43, 0, 5.82],
    scale: [1.47, 1, 1],
  };

  const tableFanPrimitiveProps = {
    object: tableFan.scene,
    rotation: [0, 1.3, 0],
    position: [-4.5, 0.07, 0.95],
    scale: [1, 1.63, 1],
  };

  // const booksPrimitiveProps = {
  //   object: books.scene,
  //   rotation: [-0.09, 1.34, 0],
  //   // position: [2.64, 1.72, 6.82],
  //   // scale: [6.15, 1.62, 1.42],
  // };

  const clockPrimitiveProps = {
    object: clock.scene,
    position: [1.47, 3, -7.2],
  };

  // gui.add(plant.scene.rotation, "x").min(-6).max(30).step(".01");
  // gui.add(plant.scene.rotation, "y").min(-3).max(3).step(".01");
  // gui.add(plant.scene.rotation, "z").min(-9).max(20).step(".01");
  // gui.add(plant.scene.scale, "x").min(-6).max(30).step(".01");
  // gui.add(plant.scene.scale, "y").min(-3).max(3).step(".01");
  // gui.add(plant.scene.scale, "z").min(-9).max(20).step(".01");
  // gui.add(plant.scene.position, "x").min(-6).max(30).step(".01");
  // gui.add(plant.scene.position, "y").min(-3).max(3).step(".01");
  // gui.add(plant.scene.position, "z").min(-9).max(20).step(".01");
  return (
    <>
      {<primitive {...buildingPrimitiveProps} />}
      <primitive {...doorPrimitiveProps} />
      <primitive {...table1PrimitiveProps} />
      <primitive {...table2PrimitiveProps} />
      <primitive {...table3PrimitiveProps} />
      <primitive {...table4PrimitiveProps} />
      <primitive {...bookTablePrimitiveProps} />
      <primitive {...chairPrimitiveProps} />
      <primitive {...chair1PrimitiveProps} />
      <primitive {...chair2PrimitiveProps} />
      <primitive {...chair3PrimitiveProps} />
      <primitive {...chair4PrimitiveProps} />
      <primitive {...chair5PrimitiveProps} />
      <primitive {...chair6PrimitiveProps} />
      <primitive {...laptopPrimitiveProps} />
      <primitive {...laptop1PrimitiveProps} />
      <primitive {...laptop2PrimitiveProps} />
      <primitive {...laptop3PrimitiveProps} />
      <primitive {...laptop4PrimitiveProps} />
      <primitive {...laptop5PrimitiveProps} />
      <primitive {...laptop6PrimitiveProps} />
      <primitive {...sofaPrimitiveProps} />
      {/* <primitive {...booksPrimitiveProps} /> */}
      <primitive {...clockPrimitiveProps} />
      {/* {<primitive {...plantPrimitiveProps} />} */}
      {/* <primitive {...plant1PrimitiveProps} /> */}
      <primitive {...tableFanPrimitiveProps} />
    </>
  );
};
