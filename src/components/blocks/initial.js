import React from "react";
import { Canvas } from "react-three-fiber";
import { Sky } from "drei";
import { Physics } from "use-cannon";
import { Ground } from "../Ground";
import { Player } from "../Player";

function Initial() {
  return (
    <Canvas shadowMap sRGB>
      <Sky sunPosition={[100, 20, 100]} />
      <ambientLight intensity={0.25} />
      <pointLight castShadow intensity={0.7} position={[100, 100, 100]} />
      <Physics gravity={[0, -120, 0]}>
        <Ground position={[0, 0.2, 0]} />
        <Player position={[16, 0, 0]} />
      </Physics>
    </Canvas>
  );
}

export default Initial;
