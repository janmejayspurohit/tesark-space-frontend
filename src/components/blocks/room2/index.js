import React from "react";
import { Canvas } from "react-three-fiber";
import { Physics } from "use-cannon";
import { Room } from "./room2";
import { Player } from "./Player";

function Room2() {
  return (
    <Canvas shadowMap sRGB>
      <pointLight castShadow intensity={1} position={[100, 100, 100]} />
      <pointLight castShadow intensity={1} position={[0, 0, 0]} /* distance={2} */ />
      <pointLight castShadow intensity={1} position={[50, 50, 50]} /* distance={2} */ />
      <pointLight castShadow intensity={1} position={[-50, -50, -50]} /* distance={2} */ />
      <pointLight castShadow intensity={1} position={[-100, -100, -100]} /* distance={2} */ />
      <Physics gravity={[0, -200, 0]}>
        <Room position={[0, 0.2, 0]} />
        <Player position={[0, 0, -9]} />
      </Physics>
    </Canvas>
  );
}

export default Room2;
