import React from "react";
import { usePlane } from "use-cannon";
import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { draco } from "drei";
import door from "../../../assets/door1.glb";
import KitchenRack from "../../../assets/kitchen_rack.gltf";
import GlassRoom from "../../../assets/fish_tank.glb";
import kitchenTable from "../../../assets/kitchen_table.glb";
import kitchenDecor from "../../../assets/kitchen_decor.glb";

export const Room = props => {
  // eslint-disable-next-line no-unused-vars
  const [ref] = usePlane(() => ({ rotation: [-Math.PI / 2, 0, 0], ...props }));
  const doorGltf = useLoader(GLTFLoader, door, draco());
  const kitchenRackGltf = useLoader(GLTFLoader, KitchenRack, draco());
  // const gui = new dat.GUI();
  const glassRoomGltf = useLoader(GLTFLoader, GlassRoom);
  const kitchenTableGltf = useLoader(GLTFLoader, kitchenTable, draco());
  const kitchenDecorGltf = useLoader(GLTFLoader, kitchenDecor, draco());

  const glassPrimitiveProps = {
    object: glassRoomGltf.scene,
    position: [0, 0, 0],
    scale: [6.99, 8.67, 18.81],
  };

  const doorPrimitiveProps = {
    object: doorGltf.scene,
    position: [-0.22, -1.3, -5.9],
    rotation: [0, 1.52, 0],
    scale: [1.73, 1.3, 1.2],
  };

  const kitchenRackGltfPrimitiveProps = {
    object: kitchenRackGltf.scene,
    position: [3.1, 0.3, -17.2],
    rotation: [0, 0, 0],
    scale: [0.75, 0.75, 0.75],
  };

  const kitchenTableGltfPrimitiveProps = {
    object: kitchenTableGltf.scene,
    position: [-1.52, -1.8, -13.87],
    rotation: [0, 0, -3.2],
    scale: [-0.16, -0.16, -0.15],
  };

  const kitchenDecorGltfPrimitiveProps = {
    object: kitchenDecorGltf.scene,
    position: [-1.52, 0, -13.87],
    rotation: [0, 0, -3.2],
    scale: [-0.16, -0.16, -0.15],
  };

/*   gui.add(kitchenDecorGltf.scene.scale, "x").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.scale, "y").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.scale, "z").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.position, "x").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.position, "y").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.position, "z").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.rotation, "x").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.rotation, "y").min(-30).max(30).step(".01");
  gui.add(kitchenDecorGltf.scene.rotation, "z").min(-30).max(30).step(".01");
 */
  return (
    <>
      <primitive {...glassPrimitiveProps} />
      <primitive {...doorPrimitiveProps} />
      <primitive {...kitchenRackGltfPrimitiveProps} />
      <primitive {...kitchenTableGltfPrimitiveProps} />
     { <primitive {...kitchenDecorGltfPrimitiveProps} />}
    </>
  );
};
