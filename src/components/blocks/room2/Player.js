import React, { useEffect, useRef, useState } from "react";
import { useSphere } from "use-cannon";
import { useThree, useFrame } from "react-three-fiber";
import { useKeyboardControls } from "../../../hooks/useKeyboardControls";
import { Vector3 } from "three";
import { FPVControls } from "../../FPVControls";
import * as THREE from "three";

const SPEED = 6;

export const Player = props => {
  const [button, setButton] = useState(false)
  const { camera } = useThree();
  const { moveForward, moveBackward, moveLeft, moveRight, jump } = useKeyboardControls();
  const [ref, api] = useSphere(() => ({
    mass: 1,
    type: "Dynamic",
    ...props,
  }));
  const velocity = useRef([0, 0, 0]);
  useEffect(() => {
    api.velocity.subscribe(v => (velocity.current = v));
  }, [api.velocity]);

  // camera.rotation.y = Math.PI / 2
  useFrame(() => {
    camera.position.copy(ref.current.position);
    const direction = new Vector3();

    const frontVector = new Vector3(0, 0, (moveBackward ? 1 : 0) - (moveForward ? 1 : 0));
    const sideVector = new Vector3((moveLeft ? 1 : 0) - (moveRight ? 1 : 0), 0, 0);

    direction.subVectors(frontVector, sideVector).normalize().multiplyScalar(SPEED).applyEuler(camera.rotation);
    api.velocity.set(direction.x, 0, direction.z);
    if (jump && Math.abs(velocity.current[1].toFixed(2)) < 0.05) {
      api.velocity.set(velocity.current[0], 8, velocity.current[2]);
    }
  });

  const doorPosition = new THREE.Vector3(-0.22, -1.3, -5.9);
  var distance = camera.position.distanceTo(doorPosition);
  useEffect(() => {
    if (distance > 4) {
      setButton(false)
    } else {
      console.log("distance111", distance)
      setButton(true)
    }
  }, [distance])

  return (
    <>
      <FPVControls />
      <mesh ref={ref} />
      {button &&
        <mesh
          onClick={() => {
            window.location.href = '/room1'
          }} position={[-0.12, -1.0, -4.9]}>
          <boxGeometry attach="geometry" width={'1'} height={'1'} />
          <meshLambertMaterial
            attach="material"
            color={"red"}
          />
        </mesh>}
    </>
  );
};