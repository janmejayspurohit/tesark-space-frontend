import React, { useEffect, useRef, useState } from "react";
import { useSphere } from "use-cannon";
import { useThree, useFrame } from "react-three-fiber";
import { FPVControls } from "./FPVControls";
import { useKeyboardControls } from "../hooks/useKeyboardControls";
import { Vector3 } from "three";
import * as THREE from "three";

const SPEED = 6;

export const Player = props => {
  const { camera } = useThree();
  const [button, setButton] = useState(false)
  const { moveForward, moveBackward, moveLeft, moveRight, jump } = useKeyboardControls();
  const [ref, api] = useSphere(() => ({
    mass: 1,
    type: "Dynamic",
    ...props,
  }));

  const velocity = useRef([0, 0, 0]);
  useEffect(() => {
    api.velocity.subscribe(v => (velocity.current = v));
  }, [api.velocity]);

  useEffect(() => {
    camera.rotation.y = Math.PI / 2;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useFrame(() => {
    camera.position.copy(ref.current.position);
    const direction = new Vector3();

    const frontVector = new Vector3(0, 0, (moveBackward ? 1 : 0) - (moveForward ? 1 : 0));
    const sideVector = new Vector3((moveLeft ? 1 : 0) - (moveRight ? 1 : 0), 0, 0);

    direction.subVectors(frontVector, sideVector).normalize().multiplyScalar(SPEED).applyEuler(camera.rotation);
    api.velocity.set(direction.x, 0, direction.z);

    if (jump && Math.abs(velocity.current[1].toFixed(2)) < 0.05) {
      api.velocity.set(velocity.current[0], 8, velocity.current[2]);
    }
  });
  const doorPosition = new THREE.Vector3(-1.88, .71, -5.9);
  var distance = camera.position.distanceTo(doorPosition);
  useEffect(() => {
    if (distance > 4) {
      setButton(false)
    } else {
      console.log("distance111", distance)
      setButton(true)
    }
  }, [distance])
  const scene = new THREE.Scene();
  const geometry = new THREE.BoxGeometry(1, 1, 1);
  const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
  const cube = new THREE.Mesh(geometry, material);
  scene.add(cube);
  return (
    <>
      <FPVControls />
      <mesh ref={ref} />
      {button &&
        <mesh
          onClick={() => {
            window.location.href = '/room1'
          }} position={[-0.88, .61, -4.9]}>
          <boxGeometry attach="geometry" width="2" height="1" depth="1" scale={[0.1, 0.1, 0,1]} />
          <meshLambertMaterial
            attach="material"
            color={"green"}
          />
        </mesh>}
    </>
  );
};