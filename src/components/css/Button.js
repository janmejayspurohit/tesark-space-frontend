import React from "react";

const Button = props => {
  return (
    <button
      {...props}
      style={{
        color: "white",
        backgroundColor: "#337ab7",
        borderColor: "#337ab7",
        borderRadius: "4px",
        padding: "8px",
        margin: "20px",
        fontWeight: "bold",
        opacity: props.disabled ? "0.4" : "1",
      }}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export default Button;
